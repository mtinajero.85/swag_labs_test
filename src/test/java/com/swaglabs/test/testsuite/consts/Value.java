package com.swaglabs.test.testsuite.consts;

public class Value {

    public static final String USER_NAME = "standard_user";
    public static final String PASSWORD = "secret_sauce";
    public static final String INVALID_USER_NAME = "user";
    public static final String INVALID_PASSWORD = "password";
    public static final String LOCKED_USER_NAME = "locked_out_user";
}
