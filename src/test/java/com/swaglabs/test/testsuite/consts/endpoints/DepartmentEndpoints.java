package com.swaglabs.test.testsuite.consts.endpoints;

public class DepartmentEndpoints {

    public static final String GET_DEPARTMENT_ENDPOINT = "https://www.mercadolibre.com.mx/menu/departments";
}