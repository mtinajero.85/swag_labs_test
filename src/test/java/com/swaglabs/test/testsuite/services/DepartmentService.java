package com.swaglabs.test.testsuite.services;

import com.swaglabs.test.testsuite.consts.endpoints.DepartmentEndpoints;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DepartmentService {

    @Step("Call to Mercado libre to check departments")
    public int getDepartmentsSize() {
        return SerenityRest.given()
                .log()
                .all()
                .when()
                .get(DepartmentEndpoints.GET_DEPARTMENT_ENDPOINT)
                .then().extract().jsonPath().getInt("departments.size()");
    }
}