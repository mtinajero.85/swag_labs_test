package com.swaglabs.test.testsuite.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class LoginPage extends PageObject {

    @FindBy(xpath = "//input[@id='user-name']")
    WebElementFacade userNameInput;
    @FindBy(id = "password")
    WebElementFacade passwordInput;
    @FindBy(id = "login-button")
    WebElementFacade logInBtn;
    @FindBy(xpath = "//h3[contains(text(),'do not match')]")
    WebElementFacade notMatchErrorMsg;
    @FindBy(xpath = "//h3[contains(text(),'locked out')]")
    WebElementFacade lockedUserErrorMsg;


    public void typeUserName(String username) {
        userNameInput.sendKeys(username);
    }

    public void typePassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void clickActionOnLoginButton() {
        logInBtn.click();
    }

    public boolean isVisibleNotMatchErrorMsg() {
        waitABit(1000);
        return notMatchErrorMsg.isVisible();
    }

    public boolean isVisibleLockedUserErrorMsg() {
        waitABit(1000);
        return lockedUserErrorMsg.isVisible();
    }
}