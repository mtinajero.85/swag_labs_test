package com.swaglabs.test.testsuite.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class ProductsPage extends PageObject {

    @FindBy(xpath = "//span[contains(text(),'Products')]")
    WebElementFacade productsTitle;
    @FindBy(xpath = "//button[contains(text(),'Add to cart')]")
    List<WebElementFacade> addToCartButton;
    @FindBy(xpath = "//div[@class='inventory_item_description']/div/button")
    List<WebElementFacade> removeToCartButton;
    @FindBy(xpath = "//span[@class='shopping_cart_badge']")
    WebElementFacade shoppingCartBadge;

    public boolean isPresentProductScreenTitle() {
        waitABit(2000);
        return productsTitle.isVisible();
    }

    public void addProductByItemNumber(int itemNumber) {
        waitABit(1000);
        addToCartButton.get(itemNumber).click();
        waitABit(1000);
    }

    public void removeProductByItemNumber(int itemNumber) {
        waitABit(1000);
        removeToCartButton.get(itemNumber).click();
    }

    public String getItemsNumberFromCart() {
        return shoppingCartBadge.getText();
    }

    public boolean shoppingCartBadgeIsVisible() {
        waitABit(500);
        return shoppingCartBadge.isVisible();
    }
}