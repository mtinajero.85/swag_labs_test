package com.swaglabs.test.testsuite.tests.login;

import static org.junit.Assert.assertTrue;

import com.swaglabs.test.testsuite.tests.TestRunner;
import org.junit.Test;

import com.swaglabs.test.testsuite.consts.Value;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTagValuesOf;

@WithTagValuesOf({ "happypath", "regression", "login" })
public class LoginNegativeTests extends TestRunner {

	@Test
	@Title("Incorrect Login with an invalid username")
	public void incorrectLoginWithInvalidUsernameTest() {
		customer.openUrl();
		customer.setUserName(Value.INVALID_USER_NAME);
		customer.setPassword(Value.PASSWORD);
		customer.clickOnLoginButton();
		assertTrue(customer.isDisplayedNotMatchErrorMsg());
	}

	@Test
	@Title("Incorrect Login with an invalid password")
	public void incorrectLoginWithInvalidPasswordTest() {
		customer.openUrl();
		customer.setUserName(Value.USER_NAME);
		customer.setPassword(Value.INVALID_PASSWORD);
		customer.clickOnLoginButton();
		assertTrue(customer.isDisplayedNotMatchErrorMsg());
	}

	@Test
	@Title("Login with a locked user")
	public void loginWithLockedUserTest() {
		customer.openUrl();
		customer.setUserName(Value.LOCKED_USER_NAME);
		customer.setPassword(Value.PASSWORD);
		customer.clickOnLoginButton();
		assertTrue(customer.isDisplayedLockedUserErrorMsg());
	}
}