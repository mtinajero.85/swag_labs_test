package com.swaglabs.test.testsuite.tests.products;

import com.swaglabs.test.testsuite.consts.Value;
import com.swaglabs.test.testsuite.tests.TestRunner;
import org.junit.Test;

import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTagValuesOf;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WithTagValuesOf({"happypath", "regression", "menu"})
public class AddProductsPositiveTests extends TestRunner {

    @Test
    @Title("Add product to shopping cart")
    public void addProductToShoppingCartTest() {
        customer.performLogin(Value.USER_NAME, Value.PASSWORD);
        customer.addProductToCart(1);
        assertTrue(customer.itemsNumberIsEqualToItemsIntoShoppingCart("1"));
    }

    @Test
    @Title("Remove product from shopping cart")
    public void removeProductToShoppingCartTest() {
        customer.performLogin(Value.USER_NAME, Value.PASSWORD);
        customer.addProductToCart(1);
        customer.removeProductFromCart(1);
        assertFalse(customer.cartItemsNumberIsVisible());
    }
}