package com.swaglabs.test.testsuite.tests.login;

import static org.junit.Assert.assertTrue;

import com.swaglabs.test.testsuite.tests.TestRunner;
import org.junit.Test;

import com.swaglabs.test.testsuite.consts.Value;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTagValuesOf;

@WithTagValuesOf({"happypath", "regression", "login"})
public class LoginPositiveTests extends TestRunner {

    @Test
    @Title("Successful Login")
    public void successFulLoginTest() {
        customer.openUrl();
        customer.setUserName(Value.USER_NAME);
        customer.setPassword(Value.INVALID_PASSWORD);
        customer.clickOnLoginButton();
        assertTrue(customer.isDisplayedProductScreen());
    }

}