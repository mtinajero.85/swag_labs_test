package com.swaglabs.test.testsuite.tests.api;

import com.swaglabs.test.testsuite.tests.TestRunner;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTagValuesOf;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@WithTagValuesOf({"happypath", "regression", "API"})
public class DepartmentsPositiveTests extends TestRunner {

    @Test
    @Title("Validate Mercado Libre departments")
    public void validateMercadoLibreDepartmentsTest() {
        assertThat(departmentService.getDepartmentsSize(), greaterThan(0));
    }
}