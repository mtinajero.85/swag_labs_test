package com.swaglabs.test.testsuite.tests;

import com.swaglabs.test.testsuite.services.DepartmentService;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.swaglabs.test.testsuite.steps.Customer;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public abstract class TestRunner {

    @Managed()
    public WebDriver driver;

    @Steps
    public Customer customer;
    @Steps
    public DepartmentService departmentService;

}