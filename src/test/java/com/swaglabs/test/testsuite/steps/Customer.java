package com.swaglabs.test.testsuite.steps;

import com.swaglabs.test.testsuite.pages.LoginPage;
import com.swaglabs.test.testsuite.pages.ProductsPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;


public class Customer extends ScenarioSteps {

    private static final long serialVersionUID = 2692841035110627232L;
    LoginPage loginPage;
    ProductsPage productsPage;


    @Step("Open web site")
    public void openUrl() {
        loginPage.open();
    }

    @Step("Set username {0}")
    public void setUserName(String username) {
        loginPage.typeUserName(username);
    }

    @Step("Set password")
    public void setPassword(String password) {
        loginPage.typePassword(password);
    }

    @Step("Click on log in button")
    public void clickOnLoginButton() {
        loginPage.clickActionOnLoginButton();
    }

    @Step("Product screen is displayed")
    public boolean isDisplayedProductScreen() {
        return productsPage.isPresentProductScreenTitle();
    }

    @Step("Error message is displayed")
    public boolean isDisplayedNotMatchErrorMsg() {
        return loginPage.isVisibleNotMatchErrorMsg();
    }

    @Step("Error message is displayed")
    public boolean isDisplayedLockedUserErrorMsg() {
        return loginPage.isVisibleLockedUserErrorMsg();
    }

    public void performLogin(String username, String password) {
        loginPage.open();
        loginPage.typeUserName(username);
        loginPage.typePassword(password);
        loginPage.clickActionOnLoginButton();
    }

    @Step("Add product to cart ")
    public void addProductToCart(int itemNumber) {
        productsPage.addProductByItemNumber(itemNumber);
    }

    @Step("Remove product from cart ")
    public void removeProductFromCart(int itemNumber) {
        productsPage.removeProductByItemNumber(itemNumber);
    }

    @Step("Validate Item is into the shopping cart ")
    public boolean itemsNumberIsEqualToItemsIntoShoppingCart(String itemsNumber) {
        return itemsNumber.equals(productsPage.getItemsNumberFromCart());
    }

    @Step("Validate item is removed")
    public boolean cartItemsNumberIsVisible() {
        return productsPage.shoppingCartBadgeIsVisible();
    }
}