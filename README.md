# Swag_Labs_Test

FEMSA challenge, implementation of test automation using serenity bdd


## Required Installation 

•	Java openjdk version "1.8.0_282"
•	Apache Maven 3.6.3 (configure environment variables)
•	Install browsers like Chrome, Firefox and Safari 
•	eclipse STS( Version: 4.9.0.RELEASE)  or intelliJ IDEA


## Usage

1. Install the required software.
2. Set IDE configurations for java sdk and maven  
3. Import as maven project into the IDE
4. Download project dependencies (cmd: mvn install)
5. To use different browsers when running the project, it can be edited into the serenity.properties file (comment the current webdriver line and 
uncomment the desired line of code by adding or removing the “#“)
6. Run the project using "mvn clean verify"  command on command line (if you want to run the project by tags add -Dtags= “tag”)
7. The Serenity Report is generated under target/site/serenity/Index.html.

### Important Note:

This framework automatically downloads and installs the appropriate driver binaries for the specified driver.
Although, the latest version of WebDriver binaries may not be available in WebDriverManager binaries.
Therefore, you have to add it manually by providing the path of the WebDriver binary using the "webdriver.browser.driver" 
system property into the serenity.properties file as shown below:  


| Browser           |                                                   System Property                                                    |
|-------------------|:--------------------------------------------------------------------------------------------------------------------:|
| Chrome            |                       webdriver.chrome.driver = src/test/resources/webdriver/chromedriver.exe                        |
| Firefox           |                        webdriver.gecko.driver = src/test/resources/webdriver/geckodriver.exe                         |
| Internet Explorer |      webdriver.ie.driver = src/test/resources/webdriver/IEDriverServer.exe                                           |
| Microsoft Edge    |                        webdriver.edge.driver = src/test/resources/webdriver/msedgedriver.exe                         |


Finally, you have to deactivate the "autodownload" system property by setting as "false" 
into the serenity.conf file 